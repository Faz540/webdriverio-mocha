class WaitUntil {
    // My number one goal when writing a automation framework, is making sure the 'intent' of the code is clear to EVERYONE.
    // The below class and class method aren't really needed.
    // WebdriverIO already allows us to wait until a certain condition is met.
    
    // However, I believe a tester with little code experience can see "WaitUntil.pageTitleEquals('Search - My Shop')"
    // ...and it should be pretty obvious what the code is doing/trying to do
    
    // Inside the WaitUntil class, we could write other 'WaitUntil' methods such as 'WaitUntil.elementIsVisibleOnpage()'
    // .. or WaitUntil.urlEquals(expectedURL);
    
    // read more about browser.waitUntil here:
    // http://webdriver.io/api/utility/waitUntil.html
    pageTitleEquals(expectedPageTitle) {
        browser.waitUntil(function () {
            return browser.getTitle() === expectedPageTitle
        }, 10000, `expected page title to equal ${expectedPageTitle}`);
    }

    elementIsVisibleOnPage(element) {
        browser.waitForVisible(element.selector);
    }
}

// Like our Page Object, exporting allows us to 'require' this file and give us access to all the 'WaitUntil' methods.
module.exports = new WaitUntil;