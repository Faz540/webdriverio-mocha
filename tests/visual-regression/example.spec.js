// Read More About Visual Regression Testing usign WebdriverIO:
//http://webdriver.io/guide/services/visual-regression.html

// Creating the actual Visual Regression function that takes the screenshot.
// If the latest screenshot is within the 'misMatchTolerance' set up in the 'wdio.visual-regression.js' file, then the test passes.
// If it's not, the test will be flagged as a failure.
function visualRegressionTest(webElement) {
    var screenshot = browser.checkElement(webElement.selector);
    return expect(screenshot[0].isWithinMisMatchTolerance).to.be.true;
};

describe('Visual Regression Testing The Home Page', function() {
    // Before each test, navigate to the Home Page
    beforeEach(function() {
        browser.url('./');
    });
    // I've configured my Visual Regression test to name the taken screenshots the test names.
    it('HomePage-SearchBar', function() {
        // If this is the first time running the test, it will simply create the 'visual-regression' directory for you.
        // Along with the 'latest' and 'original' sub-directories.
        // You will also see a screenshot of the element inside both sub-directories as there is no baseline to go off.
        // Next time you run the test, it'll capture the Search Bar element on the Home Page.
        // Then it'll compare it with the element captures in the 'visual-regression/original' directory
        // The file will be saved after the test name, so 'HomePage-Search.png'
        visualRegressionTest(HomePage.searchBar);
    });
    it('HomePage-Banner', function() {
        // These tests can only be ran in headless mode ;)
        // Normally, you wouldn't perform visual regression on content like this.
        // This type of content is normally changed, but I'm using it as an example
        visualRegressionTest(HomePage.banner);
    });
});