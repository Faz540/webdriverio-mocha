// STOP! Please read the comments in the 'homepage.spec.js' file for an explanation on writing tests and what the intent of the test is.
// Also, read the comments in the '../../pages/home.page.js' page object file as I'm sure they're beneficial too.

describe('Product Page', function() {
    beforeEach(function() {
        // As this test is about the product page, I don't feel like we need to write the code going from HomePage > Product Page
        browser.url('./index.php?id_product=1&controller=product');
    });
    it("can add a product to the user's cart", function() {
        ProductPage.addItemToCart();
        // Assign all the text on the Pop Up Cart  to 'cartPopupText' variable
        const cartPopupText = ProductPage.cartPopup.getText();
        // Expects 'Product successfully added to your shopping cart' to be shown in the Pop Up Cart 
        expect(cartPopupText).to.contain('Product successfully added to your shopping cart')
    })
});