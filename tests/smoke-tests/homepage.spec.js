describe('HomePage Tests', function() {
    // Before each test, navigate to the Home Page
    beforeEach(function() {
        browser.url('./');
    });
    it('returns the expected page title', function() {
        // Get the current page title and assign it to the variable 'title'
        const title = browser.getTitle();
        //expect the current page title to equal 'My Store'
        expect(title).to.equal('My Store');
    });
    it('performs a search and expects at least 1 result returned', function() {
        // Focus the search bar and clear any text that might be inside it.
        browser.element('#search_query_top').clearElement();
        // Focus the search bar and enter the text 'black dresses'
        browser.element('#search_query_top').setValue('black dresses');
        // Click the search button
        browser.element('.button-search').click();
        // Wait until the current page title is 'Search - My Store'
        // This will wait until we're on the results page
        browser.waitUntil(function () { return browser.getTitle() === 'Search - My Store' });
        // Riiiight...lots of explanation here - bare with me!
        // We create a variable named 'numberOfItems'
        // We get the text of the element that has the class selector 'heading-counter'
        // Assuming 5 results are returned the text value will be '5 results have been found.'
        // The only part of that text we're interested in is the first number.
        // So, we assign the first character of the above text to the 'numberOfItems' variable
        let numberOfItems = browser.getText('.heading-counter')[0];
        // Now...we're not finished.
        // Unfortunately, the '5' text value is a string. We want this to be a pure number/integer
        // So we convert the '5' to a 5. See the difference?
        numberOfItems = parseInt(numberOfItems);
        // Since numberOfItems is a number/integer, we can now write an assertion around the value.
        // We expect the number of results returned to be greater than 0
        expect(numberOfItems).to.be.greaterThan(0);
    });
    it('performs the above test, but this time using Page Objects', function() {
        HomePage.searchFor('black dresses')
        expect(ResultsPage.numberOfItems).to.be.greaterThan(0);
    });
});