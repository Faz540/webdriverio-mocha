exports.config = {
  baseUrl: 'http://automationpractice.com',
  // Default timeout for all 'wait' commands = 10 Seconds
  waitforTimeout: 10000,
  // Default timeout if server doesn't respond = 60 Seconds
  connectionRetryTimeout: 60000,
  // Location of tests
  specs: [ 
    './tests/smoke-tests/*.spec.js'
  ],
  // What browser will be used in the test
  capabilities: [ {
    // Run one '*.spec.js' at a time
    // You can increase the number to run tests in parallel
    'maxInstances': 1,
    'browserName': 'firefox',
    'moz:firefoxOptions': {
      // Uncomment the below code to run the test in headless mode!!
      //'args': ['-headless']
    },
  }],
  sync: true,
  //logLevel: 'silent',
  //coloredLogs: true,
  // If any failures occur, WebdriverIO takes a screenshot and places them in the below directory.
  screenshotPath: './errorShots/',
  // This service is required so the selenium server is ran automatically before each test run.
  services: ['selenium-standalone'],
  // Use the 'spec' test reporter (There are lots of reporters available) 
  reporters: ['spec'],
  framework: 'mocha',
  mochaOpts: {
    ui: 'bdd',
    // Fails if a page takes longer than = 30 Seconds
    timeout: 30000
  },
  // Hooks - Run the below code BEFORE each '*.spec.js' file
  before: function(capabilities, specs) {
    console.log("About To test a '*.spec.js' file")
    expect = require('chai').expect;
    // Require all your Page Objects/Browser Interations Here:
    WaitUntil = require('../browser-interactions/waits');
    HomePage = require('../pages/home.page');
    ResultsPage = require('../pages/results.page');
    ProductPage = require('../pages/product.page');
  },
  
  //  Run the below code AFTER each '*.spec.js' file
  after: function(capabilities, specs) {
    console.log("Finished testing a '*.spec.js' file")
  }
};