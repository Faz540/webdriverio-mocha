// Read More About Visual Regression Testing usign WebdriverIO:
//http://webdriver.io/guide/services/visual-regression.html

// We want to require our default config file and inherit all properties from it so we don't have create a whole new config.
var defaultConfig = require('./wdio.conf.js').config;

// Dependencies for Visual Regression Testing:
var visualRegressionCompare = require('wdio-visual-regression-service/compare');
var path = require('path');

// Creating a function that creates the file name of the screenshot taken during Visual Regression
function getScreenshotName(folder, context) {
    var testName = context.test.title;
    return path.join(process.cwd(), folder, testName + ".png");
}

var visualRegressionConfig = Object.assign(defaultConfig, {
    // When perform visual regression tests, only run the test files inside the './tests/visual-regression' directory
    specs: [
        './tests/visual-regression/*.spec.js'
    ],
    // WebdriverIO services required for Visual Regression testing
    services: ['selenium-standalone', 'visual-regression'],

    // Set up for visual regression:
    visualRegression: {
        compare: new visualRegressionCompare.LocalCompare({
            // Original/Baseline image will be placed in the 'original' directory
            referenceName: getScreenshotName.bind(null, 'visual-regression/original'),
            // Latest image will be placed in the 'latest' directory
            screenshotName: getScreenshotName.bind(null, 'visual-regression/latest'),
            // If the latest screenshot differs from the original/baselien image, it'll also be placed in the 'diff' directory
            // Any discrepancies will be highlighted in the image itself for inspection
            diffName: getScreenshotName.bind(null, 'visual-regression/diff'),
            // Number between 0 and 100 that defines the degree of mismatch to consider two images as identical.
            // Increasing this value will decrease test coverage.
            misMatchTolerance: 0.05
        })
    }
});

exports.config = visualRegressionConfig;