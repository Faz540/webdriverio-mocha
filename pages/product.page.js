// STOP! Please read the comments in the 'home.page.js' page object before reading this.
class ProductPage {
    // There isn't a 'good' selector for the 'Add To Cart' button.
    // I want to avoid using the 'submit' selector value, as pages can have several 'submit' style buttons
    // Here, I am using xPath to navigate to the 'button' inside the 'add_to_cart' ID selector (which isn't the actual button if you inspect the element in your browser!)
    get addToCart() { return $('//*[@id="add_to_cart"]/button'); }
    get cartPopup() { return $('#layer_cart'); }

    addItemToCart() {
        this.addToCart.click();
        // Wait until the Pop Up Cart  element is displayed on the screen.
        WaitUntil.elementIsVisibleOnPage(this.cartPopup);
    }
};

module.exports = new ProductPage;