// STOP! Please read the comments in the 'home.page.js' page object first
// Also, read the comments for the 'performs a search and expects at least 1 result returned' test in the 'homepage.spec.js' file.
class ResultsPage {
    // Get the first character of the element with the class 'heading-counter', then convert that text value to a number/integer
    get numberOfItems() { return parseInt(browser.getText('.heading-counter')[0]); }
};

module.exports = new ResultsPage;