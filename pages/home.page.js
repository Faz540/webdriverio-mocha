// Here is an example Page Object, or atlest, how I create my Page Objects:

// First, I create a class and name it after the page.
// In this case it's named HomePage

class HomePage {

    // Inside the HomePage object I list all the elements I want to use in my tests.
    // A good practice is to ONLY create the elements you are actually using, your page object files will become cluttered quickly.

    // The format to create the element is as follows:
    // get nameYouWantToGiveTheElement() { return $('theElementsIdentifer'); }

    //Since the Search Bar element has an ID, we'll use that:
    get searchBar() { return $('#search_query_top'); }
    // I want to use the Search Button element too, so we'll use that too:
    // This element didn't have an ID selector, but it does have a 'button-search' class 
    get searchButton() { return $('.button-search'); }
    // I'll also include the HomePage banner for future Visual Regression testing:
    get banner() { return $('.banner'); }
    
    // Notice if an element has an ID, we use '#'
    // And if an element has a class, we use '.'

    // If you want to use other selectors, feel free to use xPaths:
    // Here are some of the ways we can target the Search Bar element assuming it didn't have an ID.
    // get searchBar() { return $('//*[@name="search_query"]')}
    // get searchBar() { return $('//*[@placeholder="Search"]')}

    // Once we've declared our page elements, we can write all the actions related to the Homepage:

    searchFor(searchTerm) {
        // The below targets the searchBar element and clears it, incase it has any text inside.
        this.searchBar.clearElement();
        // Then we enter our search term into the seachBar element
        this.searchBar.setValue(searchTerm);
        // Then, we click the searchButton to submit the search
        this.searchButton.click();
        // This will wait until the page's title is 'Search - My Store' before doing anything else.
        // Notice how I'm using my own 'waitUntil.pageTitleEquals' class method from '../browser-interactions/waits.js#
        WaitUntil.pageTitleEquals('Search - My Store'); 
    };
};

// Now we export the Page Object, so requiring this file will give us access to all the HomePage elements and 'actions'
module.exports = new HomePage;